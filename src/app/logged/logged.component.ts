import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../services/security.service';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-logged',
  templateUrl: './logged.component.html',
  styleUrls: ['../css/bootstrap.min.css', '../css/style.css']
})
export class LoggedComponent {

  constructor(private security: SecurityService) {
  }

  logout() {
    this.security.logout();
  }
}
