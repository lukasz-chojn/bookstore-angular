import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { LoggedComponent } from './logged/logged.component';
import { AddBookComponent } from './books/add/add-book/add-book.component';
import { GetBookComponent } from './books/get/get-book/get-book.component';
import { DeleteBookComponent } from './books/delete/delete-book/delete-book.component';
import { LoanBookComponent } from './books/modyfi/loan/loan-book/loan-book.component';
import { ModifyComponent } from './books/modyfi/modyfi/modyfi.component';
import { ReturnBookComponent } from './books/modyfi/return-book/return-book.component';
import { ModyfiTitleComponent } from './books/modyfi/modyfi_title/modyfi-title/modyfi-title.component';
import { ModyfiDateComponent } from './books/modyfi/modyfi_date/modyfi-date/modyfi-date.component';
import { ModyfiAuthorComponent } from './books/modyfi/modyfi_author/modyfi-author/modyfi-author.component';


const routes: Routes =
  [
    {
      path: 'home', component: MainComponent
    },
    {
      path: 'login', component: LoginComponent
    },
    {
      path: 'loggedUser', component: LoggedComponent
    },
    {
      path: 'add', component: AddBookComponent
    },
    {
      path: 'get', component: GetBookComponent
    },
    {
      path: 'modify', component: ModifyComponent
    },
    {
      path: 'delete', component: DeleteBookComponent
    },
    {
      path: 'loan', component: LoanBookComponent
    },
    {
      path: 'return', component: ReturnBookComponent
    },
    {
      path: 'modyfiTitle', component: ModyfiTitleComponent
    },
    {
      path: 'modyfiDate', component: ModyfiDateComponent
    },
    {
      path: 'modyfiAuthor', component: ModyfiAuthorComponent
    }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
