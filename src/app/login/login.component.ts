import {Component, OnInit, Input} from '@angular/core';
import {SecurityService} from '../services/security.service';
import {FormGroup, FormBuilder, FormControl, Validators, NgForm} from '@angular/forms';
import {HttpErrorResponse, HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../css/bootstrap.min.css', '../css/style.css']
})
export class LoginComponent {

  message = '';

  constructor(private security: SecurityService, private router: Router) {
  }

  onSubmit(form: NgForm) {
    this.security.login(form.value)
      .subscribe(
        (data: any) => {
          localStorage.setItem('token', JSON.parse(data.token));
          this.router.navigateByUrl('/loggedUser');
        },
        (error: HttpErrorResponse) => {
          this.message = error.message;
        }
      );
  }
}
