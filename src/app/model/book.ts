export class Book {
  id: number;
  title: string;
  author: string;
  releaseDate: string;
  isLoan: boolean;
  newTitle?: string;
  newAuthor?: string;
  newReleaseDate?: string;
}
