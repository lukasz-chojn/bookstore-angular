import {Component, OnInit} from '@angular/core';
import {Book} from 'src/app/model/book';
import {BookstoreService} from 'src/app/services/bookstore.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-modyfi-date',
  templateUrl: './modyfi-date.component.html',
  styleUrls: ['../../../../css/bootstrap.min.css', '../../../../css/style.css']
})
export class ModyfiDateComponent {

  book: Book = new Book();
  message = '';

  constructor(private bookstore: BookstoreService) {
  }

  modyfi() {
    const dataToSend = JSON.stringify(this.book);
    this.bookstore.modyfiDate(dataToSend)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }

}
