import {Component, OnInit} from '@angular/core';
import {Book} from 'src/app/model/book';
import {BookstoreService} from 'src/app/services/bookstore.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-return-book',
  templateUrl: './return-book.component.html',
  styleUrls: ['../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class ReturnBookComponent {

  book: Book = new Book();
  message = '';

  constructor(private bookstore: BookstoreService) {
  }

  return() {
    const dataToSend = JSON.stringify(this.book);
    this.bookstore.return(dataToSend)
      .subscribe(
        (data: any) => {
          console.log(data);
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }

}
