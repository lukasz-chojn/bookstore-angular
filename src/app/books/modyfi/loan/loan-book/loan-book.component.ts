import {Component, OnInit} from '@angular/core';
import {Book} from 'src/app/model/book';
import {BookstoreService} from 'src/app/services/bookstore.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-loan-book',
  templateUrl: './loan-book.component.html',
  styleUrls: ['../../../../css/bootstrap.min.css', '../../../../css/style.css']
})
export class LoanBookComponent {

  book: Book = new Book();
  message = '';

  constructor(private bookstore: BookstoreService) {
  }

  loan() {
    const dataToSend = JSON.stringify(this.book);
    this.bookstore.loan(dataToSend)
      .subscribe(
        (data: any) => {
          console.log(data);
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
