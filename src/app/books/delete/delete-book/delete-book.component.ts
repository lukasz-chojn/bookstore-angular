import {Component} from '@angular/core';
import {BookstoreService} from 'src/app/services/bookstore.service';
import {NgForm} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {Book} from 'src/app/model/book';

@Component({
  selector: 'app-delete-book',
  templateUrl: './delete-book.component.html',
  styleUrls: ['../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class DeleteBookComponent {

  book: Book = new Book();
  message = '';

  constructor(private bookstore: BookstoreService) {
  }

  delete() {
    const dataToSend = JSON.stringify(this.book);
    this.bookstore.delete(dataToSend)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
