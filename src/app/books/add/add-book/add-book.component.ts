import {Component} from '@angular/core';
import {BookstoreService} from 'src/app/services/bookstore.service';
import {NgForm} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {Book} from 'src/app/model/book';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class AddBookComponent {

  book: Book = new Book();
  message: string;

  constructor(private bookstore: BookstoreService) {
  }

  onSubmit() {
    this.bookstore.add(this.book)
      .subscribe(
        (data: any) => {
          this.message = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }

}
