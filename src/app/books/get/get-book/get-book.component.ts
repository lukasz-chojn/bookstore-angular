import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {BookstoreService} from 'src/app/services/bookstore.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Book} from 'src/app/model/book';


@Component({
  selector: 'app-get-book',
  templateUrl: './get-book.component.html',
  styleUrls: ['../../../css/bootstrap.min.css', '../../../css/style.css']
})
export class GetBookComponent implements OnInit {

  books: any[] = [];
  title = '';
  message = '';


  constructor(private bookStore: BookstoreService,
              @ViewChild('clear', {static: false}) private clear: ElementRef) {
  }

  ngOnInit() {
    this.bookStore.getAll()
      .subscribe(
        (data: any) => {
          this.books = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }

  get() {
    this.clear.nativeElement.clear;
    this.bookStore.getAllBooks(this.title)
      .subscribe(
        (data: any) => {
          this.books = data;
        },
        (error: HttpErrorResponse) => {
          this.message = error.error;
        }
      );
  }
}
