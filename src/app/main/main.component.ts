import {Component, OnInit,} from '@angular/core';
import {SecurityService} from '../services/security.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['../css/bootstrap.min.css', '../css/style.css']
})
export class MainComponent implements OnInit {

  constructor(private router: Router) {
  }

  token: string;

  ngOnInit() {
    this.token = this.getToken('token');

    if (this.token != null) {

      this.router.navigateByUrl('/loggedUser');
    }
  }

  getToken(key: string): string {
    return localStorage.getItem(key);
  }
}
