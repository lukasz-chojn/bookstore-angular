import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { SecurityService } from './services/security.service';
import { LoggedComponent } from './logged/logged.component';
import { AddBookComponent } from './books/add/add-book/add-book.component';
import { GetBookComponent } from './books/get/get-book/get-book.component';
import { DeleteBookComponent } from './books/delete/delete-book/delete-book.component';
import { LoanBookComponent } from './books/modyfi/loan/loan-book/loan-book.component';
import { ModifyComponent } from './books/modyfi/modyfi/modyfi.component';
import { ReturnBookComponent } from './books/modyfi/return-book/return-book.component';
import { ModyfiTitleComponent } from './books/modyfi/modyfi_title/modyfi-title/modyfi-title.component';
import { ModyfiDateComponent } from './books/modyfi/modyfi_date/modyfi-date/modyfi-date.component';
import { ModyfiAuthorComponent } from './books/modyfi/modyfi_author/modyfi-author/modyfi-author.component';

@NgModule({
  declarations: [
    MainComponent,
    LoginComponent,
    LoggedComponent,
    AddBookComponent,
    GetBookComponent,
    DeleteBookComponent,
    LoanBookComponent,
    ModifyComponent,
    ReturnBookComponent,
    ModyfiTitleComponent,
    ModyfiDateComponent,
    ModyfiAuthorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [SecurityService],
  bootstrap: [MainComponent]
})
export class AppModule { }
