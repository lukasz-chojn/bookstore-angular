import {Injectable} from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  loginLink = 'http://localhost:8080/authenticate';

  constructor(private http: HttpClient, private router: Router) {
  }

  login(data: any): Observable<any> {
    return this.http.post<any>(this.loginLink, data,
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
      });
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/home');
  }
}
