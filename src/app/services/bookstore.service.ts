import {Injectable} from '@angular/core';
import {Observable, from} from 'rxjs';
import {HttpHeaders, HttpClient, HttpParams, HttpRequest} from '@angular/common/http';
import {Book} from '../model/book';

@Injectable({
  providedIn: 'root'
})
export class BookstoreService {

  mainLink = 'http://localhost:8080/books';
  getBookByTitle = this.mainLink + '/book';
  loanBook = this.mainLink + '/loanBook';
  returnBook = this.mainLink + '/returnBook';
  modifyTitle = this.mainLink + '/modifyTitle';
  modifyDate = this.mainLink + '/modifyDate';
  modifyAuthor = this.mainLink + '/modifyAuthor';

  headers = {
    // Authorization: 'Bearer ' + localStorage.getItem('token'),
    'Content-Type': 'application/json',
    Accept: ['application/json; charset=UTF-8', 'application/x-www-form-urlencoded']
  };

  httpHeaders = {
    headers: new HttpHeaders(this.headers)
  };

  constructor(private http: HttpClient) {
  }

  add(book: Book) {
    return this.http.post(this.mainLink, book, this.httpHeaders);
  }

  getAll(): Observable<any> {
    return this.http.get(this.mainLink, this.httpHeaders);
  }

  getAllBooks(book: string): Observable<any> {
    return this.http.get<any>(this.getBookByTitle + `?title=${book}`, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
  }

  loan(book: string) {
    return this.http.put(this.loanBook, book, this.httpHeaders);
  }

  return(book: string) {
    return this.http.put(this.returnBook, book, this.httpHeaders);
  }

  modyfiTitle(data: any): Observable<any> {
    return this.http.put(this.modifyTitle, data, this.httpHeaders);
  }

  modyfiDate(data: any): Observable<any> {
    return this.http.put(this.modifyDate, data, this.httpHeaders);
  }

  modyfiAuthor(data: any): Observable<any> {
    return this.http.put(this.modifyAuthor, data, this.httpHeaders);
  }

  delete(book: string) {
    const requestOptions = {
      headers: this.headers,
      body: book
    };
    return this.http.delete(this.mainLink, requestOptions);
  }
}
